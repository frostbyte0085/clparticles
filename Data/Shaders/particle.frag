#version 410 core

layout(location=0) out vec4 out_color;

in vec4 fColor;

void main () {
    out_color = fColor;
}