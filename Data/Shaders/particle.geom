#version 410 core

layout(points) in;
layout(triangle_strip) out;
layout(max_vertices=4) out;

in vec4 vColor[];
out vec4 fColor;

uniform mat4 projectionMatrix;

void main() {
    
    float particle_size = 1.5;
    vec4 P = gl_in[0].gl_Position;
    P.w = 1.0f;
    
    // a: left-bottom
    vec2 va = P.xy + vec2(-0.5, -0.5) * particle_size;
    gl_Position = projectionMatrix * vec4(va, P.zw);
    fColor = vColor[0];
    EmitVertex();
    
    // b: left-top
    vec2 vb = P.xy + vec2(-0.5, 0.5) * particle_size;
    gl_Position = projectionMatrix * vec4(vb, P.zw);
    fColor = vColor[0];
    EmitVertex();
    
    // d: right-bottom
    vec2 vd = P.xy + vec2(0.5, -0.5) * particle_size;
    gl_Position = projectionMatrix * vec4(vd, P.zw);
    fColor = vColor[0];
    EmitVertex();
    
    // c: right-top
    vec2 vc = P.xy + vec2(0.5, 0.5) * particle_size;
    gl_Position = projectionMatrix * vec4(vc, P.zw);
    fColor = vColor[0];
    EmitVertex();
    
    EndPrimitive();
}