#version 410 core

layout(location=0) in vec3 position;
layout(location=1) in vec4 color;

uniform mat4 worldMatrix;
uniform mat4 viewMatrix;
uniform vec2 screenSize;
out vec4 vColor;

void main() {
    
    mat4 mv = worldMatrix * viewMatrix;
    vColor = color;
    
    vec3 p = position;
    p.xy -= screenSize * 0.5f;
    p.y = -p.y;
    
    gl_Position = mv * vec4(p, 1.0);
}