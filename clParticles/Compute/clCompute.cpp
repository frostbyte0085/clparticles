//
//  clCompute.cpp
//  clParticles
//
//  Created by Pantelis Lekakis on 30/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "clCompute.h"

clCompute::clCompute(): context(nullptr), device(nullptr), platform(nullptr), queue(nullptr) {
    
}

clCompute::~clCompute() {
    shutdown();
}

void clCompute::Init() {
    cl_int error = 0;
    
    cl_uint platformCount = 0;
    cl_platform_id *platformIds = nullptr;
    
    error = clGetPlatformIDs(0, nullptr, &platformCount);
    if (error != CL_SUCCESS) {
        throw std::runtime_error("Could not find a compatible OpenCL platform!");
    }

    // just get the first one available
    if (platformCount > 0) {
        platformIds = new cl_platform_id[platformCount];
        clGetPlatformIDs(platformCount, platformIds, nullptr);
        
        platform = platformIds[0];
        delete[] platformIds;
        
        error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, nullptr);
        if (error != CL_SUCCESS) {
            throw std::runtime_error ("Could not get a compatible OpenCL device!");
        }
        
#if defined (__APPLE__) || defined(MACOSX)
        CGLContextObj kCGLContext = CGLGetCurrentContext();
        CGLShareGroupObj kCGLShareGroup = CGLGetShareGroup(kCGLContext);
        cl_context_properties properties[] = { CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE, (cl_context_properties)kCGLShareGroup, 0 };

        context = clCreateContext(properties, 0, 0, nullptr, nullptr, &error);
#elif defined _WIN32
        
#endif
        
        if (error != CL_SUCCESS) {
            throw std::runtime_error ("Could not create an OpenCL device!");
        }
        
        queue = clCreateCommandQueue(context, device, 0, &error);
        if (error != CL_SUCCESS) {
            throw std::runtime_error ("Could not create an OpenCL command queue!");
        }
    }
    
}

void clCompute::Finish() {
    clFinish(queue);
}

void clCompute::Flush() {
    clFlush(queue);
}

void clCompute::shutdown() {
    Flush();
    Finish();
    
    if (queue != nullptr) {
        clReleaseCommandQueue(queue);
    }
    queue = nullptr;
    
    if (context != nullptr) {
        clReleaseContext(context);
    }
    context = nullptr;
}