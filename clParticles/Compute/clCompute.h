//
//  clCompute.h
//  clParticles
//
//  Created by Pantelis Lekakis on 30/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __clParticles__clCompute__
#define __clParticles__clCompute__

#include "clParticles.h"

class clCompute {
public:
    clCompute();
    ~clCompute();
    
    void Init();
    
    cl_device_id& GetDevice() { return device; }
    cl_platform_id& GetPlatform() { return platform; }
    cl_context& GetContext() { return context; }
    cl_command_queue& GetQueue() { return queue; }
    
    void Flush();
    void Finish();
    
private:
    void shutdown();
    
    cl_device_id device;
    cl_platform_id platform;
    cl_context context;
    cl_command_queue queue;
};

#endif /* defined(__clParticles__clCompute__) */
