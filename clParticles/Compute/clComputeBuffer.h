//
//  clComputeBuffer.h
//  clParticles
//
//  Created by Pantelis Lekakis on 03/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __clParticles__clComputeBuffer__
#define __clParticles__clComputeBuffer__

#include "clCompute.h"
#include "clComputeObject.h"

template<class T>
class clComputeBuffer: public clComputeObject {
public:
    clComputeBuffer() {}
    ~clComputeBuffer() {
        data.clear();
    }
    
    void Init(int numberOfItems, cl_mem_flags flags = CL_MEM_READ_WRITE) {
        cl_int error = 0;
        data.resize(numberOfItems);
        
        object = clCreateBuffer(clStatics::compute->GetContext(), flags, numberOfItems * sizeof(T), nullptr, &error);
        if (error != CL_SUCCESS) {
            throw std::runtime_error ("Could not create OpenCL buffer!");
        }
    }
    
    void InitVBO (unsigned int vboId, int numberOfItems, cl_mem_flags flags = CL_MEM_READ_WRITE) {
        cl_int error = 0;
        data.resize(numberOfItems);
        
        bIsFromVBO = true;
        
        object = clCreateFromGLBuffer(clStatics::compute->GetContext(), flags, vboId, &error);
        if (error != CL_SUCCESS) {
            throw std::runtime_error ("Could not create OpenCL buffer (from VBO)!");
        }
    }
    
    void ReadAsync(int startOffsetItems, int numberOfItems, cl_event& event) {
        cl_readAsync(&data[0], startOffsetItems * sizeof(T), numberOfItems * sizeof(T), event);
    }
    
    void WriteAsync(int startOffsetItems, int numberOfItems, cl_event& event) {
        cl_writeAsync(&data[0], startOffsetItems * sizeof(T), numberOfItems * sizeof(T), event);
    }
    
    void Read(int startOffsetItems, int numberOfItems) {
        cl_read(&data[0], startOffsetItems * sizeof(T), numberOfItems * sizeof(T));
    }
    
    void Write(int startOffsetItems, int numberOfItems) {
        cl_write(&data[0], startOffsetItems * sizeof(T), numberOfItems * sizeof(T));
    }
    
    T& Get(int i) { return data[i]; }
    T& operator[](int i) { return data[i]; }
    
private:
    std::vector<T> data;
    
   void cl_read(void *dataPtr, int startOffsetBytes, int numberOfBytes) {
       Lock();
       cl_int error = clEnqueueReadBuffer(clStatics::compute->GetQueue(), object, CL_TRUE, startOffsetBytes, numberOfBytes, dataPtr, 0, nullptr, nullptr);
       Unlock();
       
       if (error != CL_SUCCESS) {
           throw std::runtime_error ("Could not read from OpenCL buffer!");
       }
   }
   
   void cl_readAsync(void *dataPtr, int startOffsetBytes, int numberOfBytes, cl_event& readEvent) {
       Lock();
       cl_int error = clEnqueueReadBuffer(clStatics::compute->GetQueue(), object, CL_FALSE, startOffsetBytes, numberOfBytes, dataPtr, 0, nullptr, &readEvent);
       Unlock();
       
       if (error != CL_SUCCESS) {
           throw std::runtime_error ("Could not read from OpenCL buffer (Async)!");
       }
   }
   
   void cl_write(void *dataPtr, int startOffsetBytes, int numberOfBytes) {
       Lock();
       cl_int error = clEnqueueWriteBuffer(clStatics::compute->GetQueue(), object, CL_TRUE, startOffsetBytes, numberOfBytes, dataPtr, 0, nullptr, nullptr);
       Unlock();
       
       if (error != CL_SUCCESS) {
           throw std::runtime_error ("Could not write to OpenCL buffer!");
       }
   }
   
   void cl_writeAsync(void *dataPtr, int startOffsetBytes, int numberOfBytes, cl_event& writeEvent) {
       Lock();
       cl_int error = clEnqueueWriteBuffer(clStatics::compute->GetQueue(), object, CL_FALSE, startOffsetBytes, numberOfBytes, dataPtr, 0, nullptr, &writeEvent);
       Unlock();
       
       if (error != CL_SUCCESS) {
           throw std::runtime_error ("Could not write to OpenCL buffer (Async)!");
       }
   }
};

#endif /* defined(__clParticles__clComputeBuffer__) */
