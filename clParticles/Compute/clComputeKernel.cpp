//
//  clComputeKernel.cpp
//  clParticles
//
//  Created by Pantelis Lekakis on 04/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "clComputeKernel.h"

clComputeKernel::clComputeKernel(cl_program program, const std::string& name) : kernel(nullptr) {
    this->program = program;
    this->name = name;
}

clComputeKernel::~clComputeKernel() {
    if (kernel != nullptr) {
        clReleaseKernel(kernel);
    }
    kernel = nullptr;
    
    clInteropArgs.clear();
}

void clComputeKernel::build() {
    cl_int error = 0;
    kernel = clCreateKernel(program, name.c_str(), &error);
    if (error != CL_SUCCESS) {
        throw std::runtime_error ("Could not create OpenCL kernel with name " + name);
    }
}

bool clComputeKernel::SetArg(int arg, void *argPtr, size_t size) {
    assert(kernel);
    
    cl_int err = clSetKernelArg(kernel, arg, size, argPtr);
    if (err != CL_SUCCESS) {
        std::cout << "Error while setting kernel argument!" << std::endl;
        return false;
    }
    
    return true;
}

bool clComputeKernel::SetArg(int arg, clComputeObject *object) {
    assert (kernel);
    
    if (object->IsFromVBO()) {
        if (find(clInteropArgs.begin(),clInteropArgs.end(), object->GetObject()) == clInteropArgs.end()){
            clInteropArgs.push_back(object->GetObject());
        }
    }
    
    return SetArg(arg, &object->GetObject(), sizeof(&object->GetObject()));
}

bool clComputeKernel::Run(int dimensions, size_t *globalSize, size_t *localSize, cl_uint eventsInWaitList, const cl_event *eventWaitList, cl_event *runEvent) {
    assert (kernel);
    
    if (!bindInterop()) {
        return false;
    }
    
    cl_int err = clEnqueueNDRangeKernel(clStatics::compute->GetQueue(), kernel, dimensions, nullptr, globalSize, localSize, eventsInWaitList, eventWaitList, runEvent);
    
    if (!unbindInterop()) {
        return false;
    }
    
    return err == CL_SUCCESS;
}

size_t clComputeKernel::roundToNextMultipleOf(size_t n, size_t divisor) const {
    assert (divisor > 0);
    
    return std::min(n % divisor, size_t(1)) * divisor + (n / divisor) * divisor;
}

bool clComputeKernel::Run1D(size_t globalSize, size_t localSize) {
    
    size_t globalSizes[1];
    
    if (localSize > 0) {
        size_t localSizes[] { localSize };
        globalSizes[0] = roundToNextMultipleOf(globalSize, localSize);
        
        Run(1, globalSizes, localSizes);
    }
    else {
        globalSizes[0] = globalSize;
        
        Run(1, globalSizes, 0);
    }
    
    return false;
}

bool clComputeKernel::bindInterop() {
    
    if (!clInteropArgs.empty()) {
        cl_int err = clEnqueueAcquireGLObjects(clStatics::compute->GetQueue(), clInteropArgs.size(), clInteropArgs.data(), 0, nullptr, nullptr);
        return err == CL_SUCCESS;
    }
    
    return false;
}

bool clComputeKernel::unbindInterop() {
    
    if (!clInteropArgs.empty()) {
        cl_int err = clEnqueueReleaseGLObjects(clStatics::compute->GetQueue(), clInteropArgs.size(), clInteropArgs.data(), 0, nullptr, nullptr);
        return err == CL_SUCCESS;
    }
    
    return false;
}