//
//  clComputeKernel.h
//  clParticles
//
//  Created by Pantelis Lekakis on 04/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __clParticles__clComputeKernel__
#define __clParticles__clComputeKernel__

#include "clCompute.h"
#include "clComputeBuffer.h"


class clComputeKernel {
    friend class clComputeProgram;
public:
    clComputeKernel(const clComputeKernel&) = delete;
    clComputeKernel& operator=(const clComputeKernel&) = delete;
    ~clComputeKernel();
    
    bool SetArg(int arg, void* argPtr, size_t size);
    bool SetArg(int arg, float v) { return SetArg(arg, &v, sizeof(v)); }
    bool SetArg(int arg, int v) { return SetArg(arg, &v, sizeof(v)); }
    bool SetArg(int arg, const glm::vec2& v) { return SetArg(arg, (void*)&v, sizeof(v)); }
    bool SetArg(int arg, const glm::vec3& v) { return SetArg(arg, (void*)&v, sizeof(v)); }
    bool SetArg(int arg, const glm::vec4& v) { return SetArg(arg, (void*)&v, sizeof(v)); }
    bool SetArg(int arg, cl_mem v) { return SetArg(arg, &v, sizeof(v)); }
    bool SetArg(int arg, clComputeObject* object);

    bool Run(int dimensions, size_t *globalSize, size_t *localSize, cl_uint eventsInWaitList=0, const cl_event *eventWaitList=nullptr, cl_event *runEvent=nullptr);
    bool Run1D (size_t globalSize, size_t localSize);
    
    std::string GetName() const { return name; }
    cl_kernel& GetKernel() { return kernel; }
    cl_program& GetProgram() { return program; }
    
private:
    clComputeKernel(cl_program program, const std::string& name);
    
    void build();
    size_t roundToNextMultipleOf (size_t n, size_t divisor) const;
    
    cl_program program;
    cl_kernel kernel;
    std::string name;
    
    std::vector<cl_mem> clInteropArgs;
    bool bindInterop();
    bool unbindInterop();
};

#endif /* defined(__clParticles__clComputeKernel__) */
