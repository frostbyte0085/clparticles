//
//  clComputeObject.cpp
//  clParticles
//
//  Created by Pantelis Lekakis on 03/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "clCompute.h"
#include "clComputeObject.h"

clComputeObject::clComputeObject(): object(nullptr), bIsLocked(false), bIsFromVBO(false) {
    
}

clComputeObject::~clComputeObject() {
    if (object != nullptr) {
        clReleaseMemObject(object);
    }
    object = nullptr;
}

void clComputeObject::Lock() {
    if (bIsLocked) return;
    if (!bIsFromVBO) return;
    
    cl_int error = clEnqueueAcquireGLObjects(clStatics::compute->GetQueue(), 1, &object, 0, nullptr, nullptr);
    if (error != CL_SUCCESS) {
        throw std::runtime_error ("Could not aquire GL object!");
    }
    clStatics::compute->Finish();
    
    bIsLocked = true;
}

void clComputeObject::Unlock() {
    if (!bIsLocked) return;
    if (!bIsFromVBO) return;
    
    cl_int error = clEnqueueReleaseGLObjects(clStatics::compute->GetQueue(), 1, &object, 0, nullptr, nullptr);
    if (error != CL_SUCCESS) {
        throw std::runtime_error ("Could not release GL object!");
    }
    clStatics::compute->Finish();
    
    bIsLocked = false;
}