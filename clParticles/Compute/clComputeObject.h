//
//  clComputeObject.h
//  clParticles
//
//  Created by Pantelis Lekakis on 03/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __clParticles__clComputeObject__
#define __clParticles__clComputeObject__

#include "../clParticles.h"

class clComputeObject {
public:
    clComputeObject(const clComputeObject&) = delete;
    clComputeObject& operator=(const clComputeObject&) = delete;
    
    virtual ~clComputeObject();
    
    cl_mem& GetObject() { return object; }
    bool IsFromVBO() const { return bIsFromVBO; }
    
    void Lock();
    void Unlock();
protected:
    clComputeObject();
    cl_mem object;
    
private:
    bool bIsLocked;
    
protected:
    bool bIsFromVBO;
};

#endif /* defined(__clParticles__clComputeObject__) */
