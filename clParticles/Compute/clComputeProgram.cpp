//
//  clComputeProgram.cpp
//  clParticles
//
//  Created by Pantelis Lekakis on 04/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "clComputeProgram.h"
#include "clComputeKernel.h"

clComputeProgram::clComputeProgram(const std::string& filename): program(nullptr) {
    this->filename = filename + ".cl";
}

clComputeProgram::~clComputeProgram() {
    clStatics::compute->Finish();
    
    if (program != nullptr) {
        clReleaseProgram(program);
    }
    program = nullptr;
}

void clComputeProgram::Build() {
    source = read(filename);
    
    cl_int error = 0;
    const char *src = source.c_str();
    
    printf ("Compiling OpenCL program %s... ", filename.c_str());
    
    program = clCreateProgramWithSource(clStatics::compute->GetContext(), 1, &src, nullptr, &error);
    if (error != CL_SUCCESS) {
        throw std::runtime_error ("Could not create program from " + filename);
    }
    
    error = clBuildProgram(program, 0, nullptr, nullptr, nullptr, nullptr);
    if (error != CL_SUCCESS) {
        throw std::runtime_error ("Could not build program");
    }
    
    printf("0 errors\n");
}

std::string clComputeProgram::read(const std::string &source) const {
    std::string contents="";
    std::fstream o(source, std::ios::in);
    if (o.is_open()) {
        std::string line;
        while (std::getline(o, line)) {
            contents += line + '\n';
        }
        contents[contents.length()] = '\0';
        o.close();
    }
    
    return contents;
}

std::shared_ptr<clComputeKernel> clComputeProgram::GetKernel(const std::string &name) {
    assert (program);
    
    std::shared_ptr<clComputeKernel> kernel = nullptr;
    
    std::unordered_map<std::string, std::shared_ptr<clComputeKernel>>::const_iterator it = kernels.find(name);
    if (it == kernels.end()) {
        kernel = std::shared_ptr<clComputeKernel>(new clComputeKernel(program, name));
        kernel->build();
        
        kernels[name] = kernel;
    }
    
    return kernel;
}