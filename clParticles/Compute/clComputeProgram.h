//
//  clComputeProgram.h
//  clParticles
//
//  Created by Pantelis Lekakis on 04/05/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __clParticles__clComputeProgram__
#define __clParticles__clComputeProgram__

#include "clCompute.h"

class clComputeKernel;

class clComputeProgram {
public:
    clComputeProgram(const std::string& filename);
    clComputeProgram(const clComputeProgram&) = delete;
    clComputeProgram& operator=(const clComputeProgram&) = delete;
    ~clComputeProgram();
    
    void Build();
    cl_program& GetProgram() { return program; }
    
    std::shared_ptr<clComputeKernel> GetKernel(const std::string& name);
    
private:
    std::string filename;
    std::string source;
    cl_program program;
    
    std::string read(const std::string& source) const;
    
    std::unordered_map<std::string, std::shared_ptr<clComputeKernel>> kernels;
};

#endif /* defined(__clParticles__clComputeProgram__) */
