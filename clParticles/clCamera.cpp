//
//  clCamera.cpp
//  clParticles
//
//  Created by Alkis Lekakis on 27/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "clCamera.h"

clCamera::clCamera() {
    SetClearColor(glm::vec4(0,0,0,1));
    
    int width, height;
    SDL_GetWindowSize(clStatics::window, &width, &height);
    
    glViewport(0, 0, width, height);
    SetupPerspective(45, (float)width/(float)height, 0.1f, 1000.0f);
}

clCamera::~clCamera() {
    
}

void clCamera::SetupPerspective(float fov, float aspect, float near, float far) {
    this->fov = fov;
    this->aspect = aspect;
    nearClipPlane = near;
    farClipPlane = far;
    bIsOrthographic = false;
    
    rebuildProjectionMatrix();
}

void clCamera::SetupOrthographic(float size, float near, float far) {
    nearClipPlane = near;
    farClipPlane = far;
    orthoSize = size;
    bIsOrthographic = true;
    
    rebuildProjectionMatrix();
}


glm::mat4 clCamera::GetProjectionMatrix() const {
    return projectionMatrix;
}

void clCamera::SetNearClipPlane(float near) {
    nearClipPlane = near;
    rebuildProjectionMatrix();
}

float clCamera::GetNearClipPlane() const {
    return nearClipPlane;
}

void clCamera::SetFarClipPlane(float far) {
    farClipPlane = far;
    rebuildProjectionMatrix();
}

float clCamera::GetFarClipPlane() const {
    return farClipPlane;
}

void clCamera::SetAspectRatio(float aspect) {
    this->aspect = aspect;
    rebuildProjectionMatrix();
}

float clCamera::GetAspectRatio() const {
    return aspect;
}

void clCamera::SetOrthographic(bool orthographic) {
    bIsOrthographic = orthographic;
    rebuildProjectionMatrix();
}

bool clCamera::IsOrthographic() const {
    return bIsOrthographic;
}

void clCamera::SetOrthographicSize(float size) {
    orthoSize = size;
    rebuildProjectionMatrix();
}

float clCamera::GetOrthographicSize() const {
    return orthoSize;
}

void clCamera::SetFOV(float fov) {
    this->fov = fov;
    rebuildProjectionMatrix();
}

float clCamera::GetFOV() const {
    return fov;
}

void clCamera::SetClearColor(const glm::vec4 color) {
    clearColor = color;
}

glm::vec4 clCamera::GetClearColor() const {
    return clearColor;
}

void clCamera::rebuildProjectionMatrix() const {
    if (bIsOrthographic) {
        projectionMatrix = glm::ortho(-orthoSize*0.5f*aspect, orthoSize*0.5f*aspect, -orthoSize*0.5f, orthoSize*0.5f, nearClipPlane, farClipPlane);
    }
    else {
        projectionMatrix = glm::perspective(fov, aspect, nearClipPlane, farClipPlane);
    }
}