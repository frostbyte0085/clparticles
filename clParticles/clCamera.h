//
//  clCamera.h
//  clParticles
//
//  Created by Alkis Lekakis on 27/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __clParticles__clCamera__
#define __clParticles__clCamera__

#include "clTransform.h"

class clCamera : public clTransform  {
public:
    clCamera();
    ~clCamera();
    
    void SetupPerspective(float fov, float aspect, float near, float far);
    void SetupOrthographic(float size, float near, float far);
    
    glm::mat4 GetProjectionMatrix() const;
    void SetNearClipPlane (float near);
    float GetNearClipPlane() const;
    
    void SetFarClipPlane(float far);
    float GetFarClipPlane() const;
    
    void SetOrthographic(bool orthographic);
    bool IsOrthographic() const;
    
    void SetFOV(float fov);
    float GetFOV() const;
    
    void SetOrthographicSize(float size);
    float GetOrthographicSize() const;
    
    void SetAspectRatio(float aspect);
    float GetAspectRatio() const;    
    
    void SetClearColor (const glm::vec4 color);
    glm::vec4 GetClearColor() const;
    
private:
    mutable glm::mat4 projectionMatrix;
    glm::vec4 clearColor;
    
    float nearClipPlane;
    float farClipPlane;
    float aspect;
    float fov;
    float orthoSize;
    bool bIsOrthographic;
    
    void rebuildProjectionMatrix() const;
};

#endif /* defined(__clParticles__clCamera__) */
