//
//  clParticles.cpp
//  clParticles
//
//  Created by Alkis Lekakis on 22/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "clParticles.h"
#include "clShader.h"
#include "clScene.h"
#include "clCamera.h"
#include "clRenderable.h"
#include "Compute/clComputeBuffer.h"
#include "Compute/clComputeProgram.h"
#include "Compute/clComputeKernel.h"

// statics
SDL_Window* clStatics::window = nullptr;
std::shared_ptr<clScene> clStatics::scene = nullptr;
std::shared_ptr<clCompute> clStatics::compute = nullptr;


clParticles::clParticles() : context(nullptr), window(nullptr), program(nullptr), kernel(nullptr) {
    deltaSeconds = 0.0f;
}

clParticles::~clParticles() {
    
}

void clParticles::Run() {
    init();
    load();
    
    SDL_StartTextInput();
    
    while (!bIsQuit) {
        Uint32 startTicks = SDL_GetTicks();
        
        SDL_Event e;
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                bIsQuit = true;
            }
            else if (e.type == SDL_TEXTINPUT) {
                handleKeys (e.text.text[0]);
            }
            else if (e.type == SDL_MOUSEMOTION) {
                int x = 0, y = 0;
                SDL_GetMouseState(&x, &y);
                mousePosition.x = x;
                mousePosition.y = y;
            }
        }
        
        update();
        render();
        
        SDL_GL_SwapWindow(window);
        
        Uint32 endTicks = SDL_GetTicks();
        deltaSeconds = static_cast<float>( (endTicks - startTicks)) / 1000.0f;
    }
    
    SDL_StopTextInput();
    
    shutdown();
}

void clParticles::update() {
    clStatics::scene->Update();
    
    kernel->SetArg(2, mousePosition);
    kernel->SetArg(3, screenSize);
    
    kernel->Run1D(kNumParticles, 0);
}

void clParticles::render() {
    compute->Finish();
    
    particleShader->SetVector2("screenSize", screenSize);
    
    clStatics::scene->Render();
}

void clParticles::init() {

    screenSize.x = kScreenWidth;
    screenSize.y = kScreenHeight;
    
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS) < 0) {
        
        throw std::runtime_error ("Could not initialise SDL");
    }
    
    if ( (window = SDL_CreateWindow( "clParticles", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, kScreenWidth, kScreenHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN)) == nullptr) {
        throw std::runtime_error ("Could not create a rendering window");
    }
    
    clStatics::window = window;

    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, kMajorVersion );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, kMinorVersion );
    
    if ( (context = SDL_GL_CreateContext(window)) == nullptr) {
        throw std::runtime_error ("Could not create a GL context");
    }
    
    compute = std::make_shared<clCompute>();
    compute->Init();
    
    clStatics::compute = compute;
    
    SDL_GL_SetSwapInterval(1);
    
    glDisable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);
    
    // some particle specific settings
    glEnable(GL_BLEND);
    //glDepthMask(GL_FALSE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    
    glewExperimental = GL_TRUE;
    GLenum error = glewInit();
    if (error != GLEW_OK) {
        throw std::runtime_error ("Could not initialize GLEW");
    }
    
    clStatics::scene = std::make_shared<clScene>();
}

void clParticles::load() {
    std::random_device rnd;
    std::mt19937 seed(rnd());
    std::normal_distribution<> distributionX(0, kScreenWidth);
    std::normal_distribution<> distributionY(0, kScreenHeight);
    std::normal_distribution<> distributionMass(0.25f, 1.0f);
    
    particleShader = std::make_shared<clShader>("Shaders/particle");
    particleShader->Build();

    camera = clStatics::scene->Create<clCamera>();
    camera->SetPosition(glm::vec3(0,0,0));
    camera->SetOrthographic(true);
    camera->SetOrthographicSize(kScreenHeight);
    clStatics::scene->SetActiveCamera(camera);
    
    triangle = clStatics::scene->Create<clRenderable>();

    std::shared_ptr<VertexBufferPosition> positions = std::make_shared<VertexBufferPosition>();
    std::shared_ptr<VertexBufferColor> colors = std::make_shared<VertexBufferColor>();
    
    for (int i=0; i<kNumParticles; i++) {
        positions->data.push_back(glm::vec3(distributionX(seed), distributionY(seed), 0.0f));
        colors->data.push_back(glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
    }
    
    triangle->SetPosition(glm::vec3(0,0,-1));
    triangle->SetInputTopology(GL_POINTS);
    triangle->SetShader(particleShader);
    triangle->SetAttribute(0, positions);
    triangle->SetAttribute(1, colors);
    triangle->Build(GL_DYNAMIC_COPY, GL_STATIC_DRAW);
    
    program = std::make_shared<clComputeProgram>("Compute/main");
    program->Build();
    
    kernel = program->GetKernel("updateParticle");
    
    particlesData_Device = std::make_shared<clComputeBuffer<clParticleData>>();
    particlesData_Device->Init(kNumParticles);
    
    particlePositions_Device = std::make_shared<clComputeBuffer<glm::vec2>>();
    particlePositions_Device->InitVBO(triangle->GetVBO(0), kNumParticles);
    
    for(int i=0; i<kNumParticles; i++) {
        clParticleData &p = particlesData_Device->Get(i);
        p.velocity = glm::vec2(0,0);
        p.mass = distributionMass(seed);
        particlePositions_Device->Get(i) = glm::vec2(positions->data[i].x, positions->data[i].y);
    }
    
    particlePositions_Device->Write(0, kNumParticles);
    particlesData_Device->Write(0, kNumParticles);
    
    kernel->SetArg(0, particlesData_Device.get());
    kernel->SetArg(1, particlePositions_Device.get());
    kernel->SetArg(2, mousePosition);
    kernel->SetArg(3, screenSize);
}

void clParticles::shutdown() {
    
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
}

void clParticles::handleKeys(unsigned char key) {
    
}