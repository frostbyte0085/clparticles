//
//  clParticles.h
//  clParticles
//
//  Created by Alkis Lekakis on 22/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __clParticles__clParticles__
#define __clParticles__clParticles__

#include <iostream>
#include <SDL2/SDL.h>

#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <vector>
#include <unordered_map>
#include <sstream>
#include <fstream>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <random>

#include "GL/glew.h"
#include <OpenCL/OpenCL.h>
#include <OpenGL/OpenGL.h>

#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtc/matrix_transform.hpp"

struct clParticleData {
    glm::vec2 velocity;
    float mass;
    float padding;
};

class clShader;
class clScene;
class clCamera;
class clRenderable;
class clCompute;
class clComputeProgram;
class clComputeKernel;
template<class T> class clComputeBuffer;

class clParticles {
public:
    clParticles();
    ~clParticles();
    
    void Run();
    
private:
    void load();
    void init();
    void shutdown();
    void handleKeys(unsigned char key);
    
    void update();
    void render();
    
    bool bIsQuit;
    SDL_Window *window;
    SDL_GLContext context;
    std::shared_ptr<clCompute> compute;
    
    const int kScreenWidth = 1280;
    const int kScreenHeight = 720;
    
    const int kMajorVersion = 4;
    const int kMinorVersion = 1;
    
    const int kNumParticles = 2000;
    
    std::shared_ptr<clShader> particleShader;
    std::shared_ptr<clCamera> camera;
    std::shared_ptr<clRenderable> triangle;
    std::shared_ptr<clComputeProgram> program;
    std::shared_ptr<clComputeKernel> kernel;
    std::shared_ptr<clComputeBuffer<clParticleData>> particlesData_Device;
    std::shared_ptr<clComputeBuffer<glm::vec2>> particlePositions_Device;
    
    float deltaSeconds;
    glm::vec2 mousePosition;
    glm::vec2 screenSize;
};

class clStatics {
    
public:
    static SDL_Window* window;
    static std::shared_ptr<clScene> scene;
    static std::shared_ptr<clCompute> compute;
};

#endif /* defined(__clParticles__clParticles__) */
