//
//  clRenderable.cpp
//  clParticles
//
//  Created by Pantelis Lekakis on 27/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "clRenderable.h"

clRenderable::clRenderable(): vao(0), ibo(0) {
    SetInputTopology(GL_TRIANGLES);
}

clRenderable::~clRenderable() {
    destroy();
    attributes.clear();
}

void clRenderable::SetAttribute (unsigned short index, std::shared_ptr<IVertexBufferAttribute> attribute) {
    attributes.insert(attributes.begin()+index, attribute);
}

std::shared_ptr<IVertexBufferAttribute> clRenderable::GetAttribute (unsigned short index) const {
    assert (index >= 0 && index < attributes.size());
    
    return attributes[index];
}

unsigned int clRenderable::GetIBO() const {
    return ibo;
}

unsigned int clRenderable::GetVAO() const {
    return vao;
}

unsigned int clRenderable::GetVBO(unsigned int index) const {
    return *(vbos.begin() + index);
}

void clRenderable::Build(int vbUsage, int ibUsage) {
    destroy();

    glGenVertexArrays (1, &vao);
    glBindVertexArray (vao);
    
    std::vector<std::shared_ptr<IVertexBufferAttribute>>::iterator it;
    unsigned int vaoIndex=0;
    for (it=attributes.begin(); it!=attributes.end(); it++) {
        std::shared_ptr<IVertexBufferAttribute> attribute = *it;
        
        // make a new vbo for this attribute
        unsigned int vboId = 0;
        glGenBuffers(1, &vboId);
        
        unsigned int count = attribute->GetComponentCount();
        size_t size = attribute->GetAttributeCount();
        size_t dataSize = attribute->GetDataSize();
        const void* data = attribute->GetDataPtr();
        
        // bind data
        glBindBuffer(GL_ARRAY_BUFFER, vboId);
        glBufferData(GL_ARRAY_BUFFER, size * dataSize, data, vbUsage);
        glVertexAttribPointer(vaoIndex, count, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(vaoIndex);
        
        // add it to our vbo list
        vbos.push_back(vboId);
        
        vaoIndex++;
    }

    // unbind this so we can safely delete our vbos
    glBindVertexArray(0);
    
    // setup our index buffer
    if (indices.size() > 0) {
        glGenBuffers(1, &ibo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]), &indices[0], ibUsage);
    }
}

int clRenderable::GetVertexCount() const {
    assert (attributes.size() > 0);
    
    return attributes[0]->GetAttributeCount();
}

GLenum clRenderable::GetInputTopology() const {
    return inputTopology;
}

void clRenderable::SetInputTopology(GLenum topology) {
    inputTopology = topology;
}

void clRenderable::SetShader(std::shared_ptr<clShader> shader) {
    assert(shader);
    this->shader = shader;
}

std::shared_ptr<clShader> clRenderable::GetShader() const {
    return shader;
}

void clRenderable::destroy() {
    for (auto v: vbos) {
        if ( v > 0) {
            glDeleteBuffers(1, &v);
        }
    }
    vbos.clear();
    
    if (vao > 0) {
        glDeleteVertexArrays(1, &vao);
    }
    vao = 0;
    
    if (ibo > 0) {
        glDeleteBuffers(1, &ibo);
    }
    ibo = 0;
}