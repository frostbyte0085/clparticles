//
//  clRenderable.h
//  clParticles
//
//  Created by Pantelis Lekakis on 27/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __clParticles__clRenderable__
#define __clParticles__clRenderable__

#include "clParticles.h"
#include "clTransform.h"

enum VertexBufferAttributeType {
    kVertexBufferAttribute_Position,
    kVertexBufferAttribute_Normal,
    kVertexBufferAttribute_Tangent,
    kVertexBufferAttribute_Texcoord,
    kVertexBufferAttribute_Color
};

// attributes
class IVertexBufferAttribute {
public:
    virtual VertexBufferAttributeType GetType()const=0;
    virtual const void* GetDataPtr()=0;
    virtual size_t GetAttributeCount()=0;
    virtual size_t GetDataSize()=0;
    
    // what kind of data do we have?
    virtual unsigned int GetComponentCount() const=0;
};

template <class T>
class VertexBufferAttribute : public IVertexBufferAttribute {
public:
    VertexBufferAttribute() { }
    virtual ~VertexBufferAttribute() { data.clear(); }
    
    size_t GetDataSize() { return sizeof(T); }
    size_t GetAttributeCount() { return data.size(); }
    const void* GetDataPtr() final {
        return data.data();
    }
    
    std::vector<T> data;
};

class VertexBufferPosition : public VertexBufferAttribute<glm::vec3> {
public:
    VertexBufferPosition(){}
    ~VertexBufferPosition(){}
    
    VertexBufferAttributeType GetType() const final { return kVertexBufferAttribute_Position; }
    unsigned int GetComponentCount() const final { return 3; }

};

class VertexBufferNormal : public VertexBufferAttribute<glm::vec3> {
public:
    VertexBufferNormal(){}
    ~VertexBufferNormal(){}
    
    VertexBufferAttributeType GetType() const final { return kVertexBufferAttribute_Normal; }
    unsigned int GetComponentCount() const final { return 3; }
};

class VertexBufferTexcoord : public VertexBufferAttribute<glm::vec2> {
public:
    VertexBufferTexcoord(){}
    ~VertexBufferTexcoord(){}
    
    VertexBufferAttributeType GetType() const final { return kVertexBufferAttribute_Texcoord; }
    unsigned int GetComponentCount() const final { return 2; }
};

class VertexBufferColor : public VertexBufferAttribute<glm::vec4> {
public:
    VertexBufferColor(){}
    ~VertexBufferColor(){}
    
    VertexBufferAttributeType GetType() const final { return kVertexBufferAttribute_Color; }
    unsigned int GetComponentCount() const final { return 4; }
};

class clRenderable: public clTransform {
public:
    clRenderable();
    clRenderable(const clRenderable& other)=delete;
    clRenderable& operator=(const clRenderable& other)=delete;
    virtual ~clRenderable();
    
    unsigned int GetIBO() const;
    unsigned int GetVAO() const;
    unsigned int GetVBO(unsigned int index) const;
    
    void SetAttribute (unsigned short index, std::shared_ptr<IVertexBufferAttribute> attribute);
    std::shared_ptr<IVertexBufferAttribute> GetAttribute (unsigned short index) const;
    
    int GetAttributeCount() const { return attributes.size(); }
    int GetVertexCount() const;
    
    void SetShader(std::shared_ptr<clShader> shader);
    std::shared_ptr<clShader> GetShader() const;
    
    GLenum GetInputTopology() const;
    void SetInputTopology(GLenum topology);
    
    std::vector<unsigned int> indices;
    
    void Build(int vbUsage, int ibUsage);
protected:
    
private:
    void destroy();
    GLenum inputTopology;
    unsigned int ibo;
    unsigned int vao;
    std::vector<unsigned int> vbos;
    std::shared_ptr<clShader> shader;
    
    std::vector<std::shared_ptr<IVertexBufferAttribute>> attributes;
    
};

#endif /* defined(__clParticles__clRenderable__) */
