//
//  clScene.cpp
//  clParticles
//
//  Created by Alkis Lekakis on 27/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "clScene.h"
#include "clCamera.h"
#include "clRenderable.h"
#include "clShader.h"

clScene::clScene() {
    
}

clScene::~clScene() {
    transforms.clear();
}

void clScene::Destroy(std::shared_ptr<clTransform> transform) {
    Destroy(transform.get());
}

void clScene::Destroy(clTransform *transform) {
    bool bFound = false;
    int i=0;
    
    for (i=0; i<transforms.size(); i++) {
        if (transforms[i].get() == transform) {
            bFound = true;
            break;
        }
    }
    
    if (bFound) {
        transforms.erase(transforms.begin() + i);
    }
}

void clScene::Update() {
    
}

void clScene::Render() {
    if (activeCamera == nullptr) return;
    
    glm::vec4 color = activeCamera->GetClearColor();
    glClearColor(color.r, color.g, color.b, color.a);
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // go through our transforms and see if we can render them
    // not the most efficient way buy meh.
    for (auto t: transforms) {
        std::shared_ptr<clRenderable> renderable = std::dynamic_pointer_cast<clRenderable>(t);
        if (renderable != nullptr) {
            renderSingle(renderable.get());
        }
    }
}

void clScene::SetActiveCamera(std::shared_ptr<clCamera> camera) {
    assert (camera);
    activeCamera = camera;
}

std::shared_ptr<clCamera> clScene::GetActiveCamera() const {
    return activeCamera;
}

void clScene::renderSingle(const clRenderable* renderable) {
    assert (renderable);
    
    std::shared_ptr<clShader> shader = renderable->GetShader();
    assert (shader);
    
    unsigned int vao = renderable->GetVAO();
    assert (vao>0);
    
    shader->Use();
    shader->SetMatrix4x4("projectionMatrix", activeCamera->GetProjectionMatrix());
    shader->SetMatrix4x4("viewMatrix", glm::inverse(activeCamera->GetWorldMatrix()));
    shader->SetMatrix4x4("worldMatrix", renderable->GetWorldMatrix());
    
    glBindVertexArray(vao);
    
    // do we have an indexbuffer? render as indexed triangles
    unsigned int ibo = renderable->GetIBO();
    if (ibo > 0) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glDrawElements(renderable->GetInputTopology(), (int)renderable->indices.size(), GL_UNSIGNED_INT, (void*)0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    // otherwise, triangles
    else {
        glDrawArrays(renderable->GetInputTopology(), 0, renderable->GetVertexCount());
        
        GLenum err = glGetError();
        int _break;
    }
    
    glBindVertexArray(0);    
    glUseProgram (0);
}