//
//  clScene.h
//  clParticles
//
//  Created by Alkis Lekakis on 27/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __clParticles__clScene__
#define __clParticles__clScene__

#include "clParticles.h"
#include "clTransform.h"

class clCamera;
class clRenderable;

class clScene {
public:
    clScene();
    ~clScene();

    template<class T, class... Args>
    std::shared_ptr<T> Create(const Args&... args) {
        auto instance = std::make_shared<T>(args...);
        std::shared_ptr<clTransform> transform = std::dynamic_pointer_cast<clTransform>(instance);
        if (transform != nullptr) {
            transform->parentScene = this;
        }
        
        transforms.push_back(std::dynamic_pointer_cast<clTransform>(instance));
        
        return instance;
    }
    
    void Destroy(std::shared_ptr<clTransform> transform);
    void Destroy(clTransform* transform);
    
    void Update();
    void Render();
    
    void SetActiveCamera(std::shared_ptr<clCamera> camera);
    std::shared_ptr<clCamera> GetActiveCamera() const;
    
private:
    std::vector<std::shared_ptr<clTransform>> transforms;
    std::shared_ptr<clCamera> activeCamera;
    
    void renderSingle(const clRenderable* renderable);
};

#endif /* defined(__clParticles__clScene__) */
