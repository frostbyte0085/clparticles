//
//  clShader.cpp
//  clParticles
//
//  Created by Alkis Lekakis on 27/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "clShader.h"

clShader::clShader(const std::string& filename) : programId(0), vsId(0), fsId(0), gsId(0) {
    vsFilename = filename + ".vert";
    fsFilename = filename + ".frag";
    gsFilename = filename + ".geom";
}

clShader::~clShader() {
    glUseProgram(0);
    
    if (gsId > 0) {
        glDeleteShader (gsId);
    }
    glDeleteShader(vsId);
    glDeleteShader(fsId);
    glDeleteProgram(programId);
    
    gsId = vsId = fsId = programId = 0;
}

unsigned int clShader::GetID() const {
    return programId;
}

void clShader::Build() {
    vsSource = read(vsFilename);
    fsSource = read(fsFilename);
    gsSource = read(gsFilename);

    vsId = buildShaderFromSource(vsFilename, vsSource, GL_VERTEX_SHADER);
    fsId = buildShaderFromSource(fsFilename, fsSource, GL_FRAGMENT_SHADER);
    if (gsSource.size() > 0) {
        gsId = buildShaderFromSource(gsFilename, gsSource, GL_GEOMETRY_SHADER);
    }
    
    programId = glCreateProgram();
    glAttachShader(programId, vsId);
    glAttachShader(programId, fsId);
    if (gsId > 0) {
        glAttachShader(programId, gsId);
    }
    printf ("Linking program...");
    
    glLinkProgram(programId);
    printProgramInfo(programId);
}

std::string clShader::read(const std::string &source) const {
    std::string contents="";
    std::fstream o(source, std::ios::in);
    if (o.is_open()) {
        std::string line;
        while (std::getline(o, line)) {
            contents += line + '\n';
        }
        contents[contents.length()] = '\0';
        o.close();
    }
    
    return contents;
}

unsigned int clShader::buildShaderFromSource(const std::string &name, const std::string &source, unsigned int type) {
    unsigned int id = glCreateShader(type);
    
    const char *src = source.c_str();
    printf ("Compiling shader %s...", name.c_str());
    glShaderSource (id, 1, &src, nullptr);
    glCompileShader(id);
    printShaderInfo(id);
    
    return id;
}

void clShader::Use() {
    glUseProgram (programId);
}

void clShader::printProgramInfo(unsigned int id) {
    int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;
    
    glGetProgramiv(id, GL_INFO_LOG_LENGTH,&infologLength);
    
    if (infologLength > 0)
    {
        infoLog = (char *)malloc(infologLength);
        glGetProgramInfoLog(id, infologLength, &charsWritten, infoLog);
        printf("\n%s\n",infoLog);
        free(infoLog);
    }
    else {
        printf(" 0 errors\n");
    }
}

void clShader::printShaderInfo(unsigned int id) {
    int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;
    
    glGetShaderiv(id, GL_INFO_LOG_LENGTH,&infologLength);
    
    if (infologLength > 0)
    {
        infoLog = (char *)malloc(infologLength);
        glGetShaderInfoLog(id, infologLength, &charsWritten, infoLog);
        printf("\n%s\n",infoLog);
        free(infoLog);
    }
    else {
        printf (" 0 errors\n");
    }
}

void clShader::SetFloat(const std::string &name, float f) {
    int location = glGetUniformLocation(programId, name.c_str());
    glProgramUniform1f(programId, location, f);
}

void clShader::SetInt(const std::string &name, int i) {
    int location = glGetUniformLocation(programId, name.c_str());
    glProgramUniform1i(programId, location, i);
}

void clShader::SetVector2(const std::string &name, const glm::vec2 &v) {
    int location = glGetUniformLocation(programId, name.c_str());
    glProgramUniform2fv(programId, location, 1, glm::value_ptr(v));
}

void clShader::SetVector3(const std::string &name, const glm::vec3 &v) {
    int location = glGetUniformLocation(programId, name.c_str());
    glProgramUniform3fv(programId, location, 1, glm::value_ptr(v));
}

void clShader::SetVector4(const std::string &name, const glm::vec4 &v) {
    int location = glGetUniformLocation(programId, name.c_str());
    glProgramUniform4fv(programId, location, 1, glm::value_ptr(v));
}

void clShader::SetMatrix4x4(const std::string &name, const glm::mat4 &m) {
    int location = glGetUniformLocation(programId, name.c_str());
    glProgramUniformMatrix4fv(programId, location, 1, GL_FALSE, glm::value_ptr(m));
}

void clShader::SetMatrix3x3(const std::string &name, const glm::mat3 &m) {
    int location = glGetUniformLocation(programId, name.c_str());
    glProgramUniformMatrix3fv(programId, location, 1, GL_FALSE, glm::value_ptr(m));
}