//
//  clShader.h
//  clParticles
//
//  Created by Alkis Lekakis on 27/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __clParticles__clShader__
#define __clParticles__clShader__

#include "clParticles.h"

class clShader {
public:
    clShader(const std::string& filename);
    ~clShader();
    
    void Build();
    void Use();
    unsigned int GetID() const;
    
    // uniforms
    void SetVector2(const std::string& name, const glm::vec2 &v);
    void SetVector3(const std::string& name, const glm::vec3 &v);
    void SetVector4(const std::string& name, const glm::vec4 &v);
    void SetFloat(const std::string& name, float f);
    void SetInt(const std::string& name, int i);
    void SetMatrix3x3(const std::string& name, const glm::mat3 &m);
    void SetMatrix4x4(const std::string& name, const glm::mat4 &m);
    
private:
    unsigned int programId;
    unsigned int vsId;
    unsigned int fsId;
    unsigned int gsId;
    
    std::string vsFilename;
    std::string fsFilename;
    std::string gsFilename;
    
    std::string vsSource;
    std::string fsSource;
    std::string gsSource;
    
    std::string read(const std::string& source) const;
    unsigned int buildShaderFromSource(const std::string& name, const std::string& source, unsigned int type);
    void printShaderInfo(unsigned int id);
    void printProgramInfo(unsigned int id);
};

#endif /* defined(__clParticles__clShader__) */
