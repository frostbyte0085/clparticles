//
//  clTransform.cpp
//  clParticles
//
//  Created by Alkis Lekakis on 27/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "clTransform.h"
#include "clScene.h"

clTransform::clTransform() {
    
}

clTransform::~clTransform() {
    if (parentScene != nullptr) {
        parentScene->Destroy(this);
    }
}

void clTransform::SetPosition(const glm::vec3 &position) {
    this->position = position;
    
    rebuildWorldMatrix();
}

glm::vec3 clTransform::GetPosition() const {
    return position;
}

void clTransform::SetRotation(const glm::quat &rotation) {
    this->rotation = rotation;
    
    rebuildWorldMatrix();
}

glm::quat clTransform::GetRotation() const {
    return rotation;
}

void clTransform::SetLookAt(const glm::vec3 &target, const glm::vec3 &up) {
    glm::mat4 lookMatrix = glm::lookAt(position, target, up);
    rotation = glm::quat_cast(lookMatrix);
}

void clTransform::SetLookAt(clTransform *target, const glm::vec3 &up) {
    assert(target != nullptr);
    
    SetLookAt(target->GetPosition(), up);
}

glm::mat4 clTransform::GetWorldMatrix() const {
    rebuildWorldMatrix();
    return worldMatrix;
}

void clTransform::rebuildWorldMatrix() const {
    translationMatrix = glm::translate(position);
    scaleMatrix = glm::scale(glm::vec3(1.0f, 1.0f, 1.0f));
    rotationMatrix = glm::mat4_cast(this->rotation);
    
    worldMatrix = translationMatrix * rotationMatrix * scaleMatrix;
}

glm::vec3 clTransform::GetForward() const {
    glm::vec4 v = rotationMatrix * glm::vec4(0,0,-1,0);
    return glm::vec3(v.x, v.y, v.z);
}

glm::vec3 clTransform::GetRight() const {
    glm::vec4 v = rotationMatrix * glm::vec4(1,0,0,0);
    return glm::vec3(v.x, v.y, v.z);
}

glm::vec3 clTransform::GetUp() const {
    glm::vec4 v = rotationMatrix * glm::vec4(0,1,0,0);
    return glm::vec3(v.x, v.y, v.z);
}