//
//  clTransform.h
//  clParticles
//
//  Created by Alkis Lekakis on 27/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#ifndef __clParticles__clTransform__
#define __clParticles__clTransform__

#include "clParticles.h"

class clTransform {
    friend class clScene;
public:
    clTransform();
    virtual ~clTransform();
    
    void SetPosition (const glm::vec3 &position);
    glm::vec3 GetPosition() const;
    
    void SetRotation (const glm::quat &rotation);
    glm::quat GetRotation() const;
    
    void SetLookAt (const glm::vec3 &target, const glm::vec3 &up);
    void SetLookAt (clTransform* target, const glm::vec3 &up);
    
    glm::mat4 GetWorldMatrix() const;
    
    glm::vec3 GetForward() const;
    glm::vec3 GetRight() const;
    glm::vec3 GetUp() const;
    
protected:
    mutable glm::mat4 worldMatrix;
    
private:
    glm::vec3 position;
    glm::quat rotation;
    
    mutable glm::mat4 rotationMatrix;
    mutable glm::mat4 scaleMatrix;
    mutable glm::mat4 translationMatrix;
    
    void rebuildWorldMatrix() const;
    
    clScene* parentScene;
    
};

#endif /* defined(__clParticles__clTransform__) */
