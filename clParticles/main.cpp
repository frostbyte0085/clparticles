//
//  main.cpp
//  clParticles
//
//  Created by Alkis Lekakis on 22/04/2015.
//  Copyright (c) 2015 Alkis Lekakis. All rights reserved.
//

#include "clParticles.h"

int main(int argc, const char * argv[]) {

    try {
        clParticles init;
        init.Run();
    }
    catch (std::runtime_error e) {
        std::cout << e.what() << std::endl;
    }
    
    return 0;
}
